plugins {
    kotlin("multiplatform") version "1.8.10"

    // List of releases: https://gitlab.com/opensavvy/gradle-vite-plugin/-/releases
    id("opensavvy.vite") // version "REPLACE THIS"
}

repositories {
    mavenCentral()
}

kotlin {
    js(IR) {
        browser()
        binaries.executable()
    }
}

vite {
    withKotlinDefaults()
}

//region Ad-hoc configuration

val jsWorkspace = "${rootProject.buildDir}/js"
val jsProjectDir = "$jsWorkspace/packages/${project.name}"

val kotlinNodeJsSetup by rootProject.tasks.getting(org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsSetupTask::class)
val kotlinNpmInstall by rootProject.tasks.getting(org.jetbrains.kotlin.gradle.targets.js.npm.tasks.KotlinNpmInstallTask::class)

val jsProductionExecutableCompileSync by tasks.getting(Task::class)

val customViteRun by tasks.registering(Exec::class) {
    description = "Starts the development web server"
    group = "vite ad-hoc"

    workingDir = file(jsProjectDir)
    commandLine(
        "${kotlinNodeJsSetup.destination}/bin/node",
        "${file(jsWorkspace)}/node_modules/vite/bin/vite.js",
        "dev",
    )

    dependsOn(
        kotlinNodeJsSetup,
        kotlinNpmInstall,
        jsDevelopmentExecutableCompileSync,
    )
}

val jsDevelopmentExecutableCompileSync by tasks.getting(Task::class) {
    dependsOn(
        tasks.configureVite,
    )
}

val customViteBuild by tasks.registering(Exec::class) {
    description = "Compiles the production web server with Rollup"
    group = "vite ad-hoc"

    workingDir = file(jsProjectDir)
    commandLine(
        "${kotlinNodeJsSetup.destination}/bin/node",
        "${file(jsWorkspace)}/node_modules/vite/bin/vite.js",
        "build",
    )

    dependsOn(
        kotlinNodeJsSetup,
        kotlinNpmInstall,
        tasks.configureVite,
        jsProductionExecutableCompileSync,
    )
}

//endregion

val build by tasks.getting {
    dependsOn(customViteBuild)
}
