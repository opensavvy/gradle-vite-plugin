plugins {
	// List of releases: https://gitlab.com/opensavvy/gradle-vite-plugin/-/releases
	id("opensavvy.vite") // version "REPLACE THIS"
}

val build by tasks.registering {
	dependsOn(tasks.named("configureVite"))

	doLast {
		println("Will be implemented in the future.")
	}
}
