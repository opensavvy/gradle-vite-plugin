# Gradle Vite Plugin

> This project has been archived in favor of the [Kotlin Vite Plugin](https://gitlab.com/opensavvy/kotlin-vite).

A [Gradle](https://gradle.org/) plugin to use the [Vite](https://vitejs.dev/) frontend environment.

## Licensing and contribution

The project is distributed under the Apache 2.0 license. The full text is available in
the [LICENSE.txt file](LICENSE.txt).

To learn how to contribute, [please read our wiki](https://gitlab.com/opensavvy/wiki/-/blob/main/README.md).
