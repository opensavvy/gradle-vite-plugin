plugins {
	`kotlin-dsl`
}

repositories {
	mavenCentral()
}

gradlePlugin {
	plugins {
		create("vite") {
			id = "opensavvy.vite"
			implementationClass = "opensavvy.gradle.vite.VitePlugin"
		}
	}
}

dependencies {
	compileOnly("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.20")
}
