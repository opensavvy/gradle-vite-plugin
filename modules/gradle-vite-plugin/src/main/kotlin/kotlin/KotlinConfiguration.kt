package opensavvy.gradle.vite.kotlin

import opensavvy.gradle.vite.config.ViteRootConfig
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByName
import org.jetbrains.kotlin.gradle.dsl.KotlinJsProjectExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

internal fun preconfigureForKotlin(config: ViteRootConfig, project: Project) = with(config) {
    //TODO: Extract the paths from the Kotlin plugin instead of hardcoding them
    config.websiteRoot.convention(project.layout.buildDirectory.dir("js/packages/${project.name}"))
    config.root.convention(config.websiteRoot.dir("kotlin"))

    project.tasks.named("configureVite") {
        dependsOn("jsProductionExecutableCompileSync")
    }

    config.plugin("@originjs/vite-plugin-commonjs", "viteCommonjs", "1.0.3", isNamedExport = true)
    config.plugin("@rollup/plugin-commonjs", "commonjs", "24.0.1")

    project.extensions.findByType(KotlinMultiplatformExtension::class.java)
        ?.apply { configureKotlinExtension(config, "jsMain") }
        ?: project.logger.warn("Vite plugin: could not find the 'kotlin' block. Some features may not be configured correctly.")
}

private fun KotlinProjectExtension.configureKotlinExtension(config: ViteRootConfig, jsSourceSetName: String) {
    sourceSets.getByName(jsSourceSetName, KotlinSourceSet::class) {
        dependencies {
            implementation(devNpm("vite", config.version.get()))

            for (plugin in config.plugins.getOrElse(emptyList())) {
                implementation(devNpm(plugin.packageName, plugin.version))
            }
        }
    }
}
