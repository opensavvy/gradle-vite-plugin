package opensavvy.gradle.vite

import opensavvy.gradle.vite.config.ViteConfig
import opensavvy.gradle.vite.config.ViteConfigWriter
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.task

@Suppress("unused") // it's injected by Gradle into a user's build
class VitePlugin : Plugin<Project> {
	override fun apply(target: Project) {

		val config = ViteConfig.createExtensionsOn(target)

		target.task("dumpViteConfiguration") {
			description = "Prints the current Vite configuration to the standard output"
			group = "vite"

			doLast {
				println(config)
			}
		}

		target.task<ViteConfigWriter>("configureVite") {
			setDefaultsFrom(config)
		}
	}
}
