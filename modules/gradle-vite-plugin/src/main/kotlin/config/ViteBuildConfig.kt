package opensavvy.gradle.vite.config

import org.gradle.api.provider.Property
import javax.inject.Inject

@Suppress("LeakingThis")
abstract class ViteBuildConfig @Inject constructor(
    @Suppress("UNUSED_PARAMETER") root: ViteRootConfig,
) {

    /**
     * Browser compatibility target for the final bundle.
     *
     * For more information, see [the official documentation](https://vitejs.dev/config/build-options.html#build-target).
     */
    abstract val target: Property<String>

    init {
        target.convention("modules")
    }

    override fun toString() = """
        *** Build configuration ***
        Target . ${target.get()}
    """.trimIndent()

}
