package opensavvy.gradle.vite.config

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

/**
 * Gradle task to convert the values provided to [ViteConfig] into a proper `vite.config.js` file.
 */
abstract class ViteConfigWriter : DefaultTask() {

    /** See [ViteRootConfig.websiteRoot]. */
    @get:InputDirectory
    abstract val websiteRoot: DirectoryProperty

    /** See [ViteRootConfig.root]. */
    @get:InputDirectory
    abstract val projectRoot: DirectoryProperty

    /** See [ViteRootConfig.base]. */
    @get:InputDirectory
    abstract val projectBase: DirectoryProperty

    /** See [ViteRootConfig.plugins]. */
    @get:Input
    abstract val plugins: ListProperty<NpmVitePlugin>

    /** See [ViteBuildConfig.target]. */
    @get:Input
    abstract val buildTarget: Property<String>

    /**
     * The path to the `vite.config.js` file which will be created by this task.
     *
     * By default, it is created in the [websiteRoot] directory.
     */
    @get:OutputFile
    abstract val configurationFile: RegularFileProperty

    init {
        description = "Generates the Vite configuration file from the Gradle DSL"
        group = "vite"
    }

    internal fun setDefaultsFrom(config: ViteConfig) {
        // Root
        websiteRoot.convention(config.root.websiteRoot)
        configurationFile.convention(websiteRoot.file("vite.config.js"))
        projectRoot.convention(config.root.root)
        projectBase.convention(config.root.base)
        plugins.convention(config.root.plugins)

        // Build
        buildTarget.convention(config.build.target)
    }

    private fun pluginImport(plugin: NpmVitePlugin) = if (plugin.isNamedExport)
        "import {${plugin.exportedAs}} from '${plugin.packageName}'"
    else
        "import ${plugin.exportedAs} from '${plugin.packageName}'"

    @TaskAction
    fun create() {
        val output = configurationFile.get().asFile
        output.writeText( //language=JavaScript
            """
            ${
                plugins.get().joinToString(separator = "\n            ") { pluginImport(it) }
            }

            /** @type {import('vite').UserConfig} */
            export default {
                root: '${projectRoot.get().asFile}',
                base: '${projectBase.get().asFile}',
                plugins: [
                    ${
                plugins.get()
                    .joinToString(separator = ",\n                    ") { "${it.exportedAs}(${it.configuration ?: ""})" }
            }
                ],           
                build: {
                    target: '${buildTarget.get()}' 
                }
            }
            
        """.trimIndent()
        )
    }
}
