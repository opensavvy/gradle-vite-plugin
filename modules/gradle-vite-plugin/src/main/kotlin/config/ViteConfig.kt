package opensavvy.gradle.vite.config

import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.create

/**
 * Wrapper for the various blocks of the `vite` extension.
 */
internal class ViteConfig(
    val root: ViteRootConfig,
    val build: ViteBuildConfig,
) {

    override fun toString() = listOf(root, build)
        .joinToString(separator = "\n\n")

    companion object {
        internal fun createExtensionsOn(project: Project): ViteConfig {
            val root = project.extensions
                .create<ViteRootConfig>("vite", project)

            val rootExtensions = (root as ExtensionAware).extensions

            val build = rootExtensions
                .create<ViteBuildConfig>("build", root)

            return ViteConfig(
                root,
                build,
            )
        }
    }
}
