package opensavvy.gradle.vite.config

import opensavvy.gradle.vite.kotlin.preconfigureForKotlin
import org.gradle.api.Project
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.intellij.lang.annotations.Language
import javax.inject.Inject

@Suppress("LeakingThis") // Gradle implements this class internally, we don't have a choice
abstract class ViteRootConfig @Inject constructor(
    private val project: Project,
) {

    /**
     * The version of the Vite package used by this build.
     *
     * The list of versions is available [on the NPM website](https://www.npmjs.com/package/vite?activeTab=versions).
     */
    abstract val version: Property<String>

    /**
     * The root directory in which all Vite commands are executed, and where the configuration is created.
     *
     * This option is added by the Gradle plugin, and doesn't exist in Vite.
     * It corresponds to the folder in which you would run Vite.
     */
    abstract val websiteRoot: DirectoryProperty

    /**
     * The project root directory, in which the `index.html` is located.
     *
     * For more information, see [the official documentation](https://vitejs.dev/config/shared-options.html#root) and
     * the [official guide](https://vitejs.dev/guide/#index-html-and-project-root).
     */
    abstract val root: DirectoryProperty

    /**
     * The base public path from which assets are served during development and production.
     *
     * For more information, see the [official documentation](https://vitejs.dev/config/shared-options.html#base) and
     * the [official guide](https://vitejs.dev/guide/build.html#public-base-path).
     */
    abstract val base: DirectoryProperty

    /**
     * The list of NPM plugins imported by this project.
     *
     * To easily add values to this property, we provide [plugin] helper function.
     *
     * For more information on plugins, see [NpmVitePlugin].
     */
    abstract val plugins: ListProperty<NpmVitePlugin>

    /**
     * The defaults for the plugin.
     *
     * We want them to be as close as possible to the [upstream Vite defaults](https://vitejs.dev/config/),
     * to make it easier for developers to get started and convert their projects.
     */
    init {
        version.convention("4.1.4")
        websiteRoot.convention(project.layout.projectDirectory)
        root.convention(websiteRoot)
        base.convention(websiteRoot)
        plugins.convention(emptyList())
    }

    /**
     * Returns a human-readable textual representation of the configured properties.
     *
     * The plugin automatically creates the task `dumpViteConfiguration` to print this to the console.
     */
    override fun toString() = """
        *** Top-level properties ***
        Vite version . ${version.get()}
        Website root . ${websiteRoot.get()}
        Project root . ${root.get()}
        Assets base .. ${base.get()}
        Plugins ...... ${
        plugins.get()
            .takeIf { it.isNotEmpty() }
            ?.joinToString(separator = "\n                       ")
            ?: "No plugins declared"
    }
    """.trimIndent()

    /**
     * Replaces the Vite default configuration by one fit for the Kotlin/JS compiler.
     *
     * You should only call this method if you have applied the `kotlin("js")` or `kotlin("multiplatform")` plugin
     * to this project.
     */
    @Suppress("unused") // IDEA doesn't see the examples
    fun withKotlinDefaults() = preconfigureForKotlin(this, project)

    //region Helpers

    /**
     * Imports a plugin from NPM.
     *
     * This is a helper function to add an element to [plugins].
     * For more information on the different parameters, see [NpmVitePlugin].
     *
     * ### Example
     *
     * ```kotlin
     * vite {
     *     plugin("@originjs/vite-plugin-commonjs", "viteCommonjs", "1.0.3")
     * }
     * ```
     *
     * @param packageName The NPM package name. See [NpmVitePlugin.packageName].
     * @param exportedAs The JS symbol exported by the plugin. See [NpmVitePlugin.exportedAs].
     * @param version The version of the plugin on NPM. See [NpmVitePlugin.version].
     * @param configuration Optional additional configuration. See [NpmVitePlugin.configuration].
     */
    fun plugin(
        packageName: String,
        exportedAs: String,
        version: String,
        @Language("JavaScript") configuration: String? = null,
        isNamedExport: Boolean = false,
    ) {
        plugins.add(
            NpmVitePlugin(
                exportedAs = exportedAs,
                packageName = packageName,
                version = version,
                configuration = configuration,
                isNamedExport = isNamedExport,
            )
        )
    }

    //endregion
}
